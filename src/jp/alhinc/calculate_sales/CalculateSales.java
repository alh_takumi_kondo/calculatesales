package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String TOTAL_OVER = "合計金額が10桁を超えました";
	private static final String FILE_COMMODITY = "商品定義ファイル";
	private static final String FILE_BRANCH = "支店定義ファイル";
	private static final String BRANCH_CODE_IS_INCORRECT = "の支店コードが不正です";
	private static final String FORMAT_IS_INVALID = "のフォーマットが不正です";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String COMMODITY_CODE_IS_INCORRECT = "の商品コードが不正です";

	//正規表現
	private static final String BRANCH_CODE_ERROR = "^[0-9]{3}$";
	private static final String COMMODITY_ERROR = "^[A-Za-z0-9]{8}$";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名
		Map<String, String> commodityCodes = new HashMap<>();
		// 商品コードと売上額
		Map<String, Long> commodityNames = new HashMap<>();
		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, FILE_BRANCH, BRANCH_CODE_ERROR)) {
			return;
		}
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityCodes, commodityNames, FILE_COMMODITY, COMMODITY_ERROR)) {
			return;
		}
		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		//ファイルが数字8桁か確認、trueならrcdFilesに格納
		for(int i = 0; i < files.length; i++) {
			String salesName;
			salesName = files[i].getName();
			if(files[i].isFile() && salesName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		//ソートの処理
		Collections.sort(rcdFiles);
		//連番か確認
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			if((latter - former) != 1) {
				System.out.println(NOT_SERIAL_NUMBER);
				return;
			}
		}

		BufferedReader br = null;
		//売上ファイルを読み込む
		for(int i = 0; i < rcdFiles.size(); i++) {
			//ファイルの中身に対する例外処理
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				//売上ファイルの中身を抽出
				List<String> rcdLine = new ArrayList<>();
				String line;
				// 一行ずつ読み込む
				while((line = br.readLine()) != null) {
					rcdLine.add(line);
				}
				if(rcdLine.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + FORMAT_IS_INVALID);
					return;
				}
				if(!branchNames.containsKey(rcdLine.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + BRANCH_CODE_IS_INCORRECT);
					return;
				}
				if(!commodityNames.containsKey(rcdLine.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + COMMODITY_CODE_IS_INCORRECT);
				}
				if(!rcdLine.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				//売上金額をlong型にキャスト
				long rcdLineSale = Long.parseLong(rcdLine.get(2));
				Long saleAmount = branchSales.get(rcdLine.get(0)) + rcdLineSale;
				Long saleAmountCommodity = commodityNames.get(rcdLine.get(1)) + rcdLineSale;
				if(saleAmount >= 10000000000L || saleAmountCommodity >= 10000000000L) {
					System.out.println(TOTAL_OVER);
					return;
				}
				//売上金額を加算
				branchSales.put(rcdLine.get(0), saleAmount);
				commodityNames.put(rcdLine.get(1), saleAmountCommodity);


			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityCodes, commodityNames)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales, String fileType, String matches) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//定義ファイルの有無
			if(!file.exists()) {
				System.out.println(fileType + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				//,区切りで改行
				String[] names = line.split(",");
				//フォーマットの確認
				if((names.length != 2) || (!names[0].matches(matches))) {
					System.out.println(fileType + FORMAT_IS_INVALID);
					return false;
				}
				//Mapに格納
				branchNames.put(names[0], names[1]);
				branchSales.put(names[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			//拡張for文で集計ファイルの出力
			for(String key: branchSales.keySet()) {
				bw.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}

